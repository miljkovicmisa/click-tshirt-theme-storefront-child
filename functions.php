<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

/* Custom scripts and styles */

function ct_custom_scripts() {
  wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap', array(), '1.1', 'all');
  wp_enqueue_script( 'custom-scripts', get_stylesheet_directory_uri() . '/ct-scripts/all-scripts.js', array ( 'jquery' ), 1.1, true);

}
add_action( 'wp_enqueue_scripts', 'ct_custom_scripts' );

/* Function that prints all woocommerce categories */

function ct_product_categories_as_menu() {
  $args = array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'parent'   => 0
  );
  $product_cat = get_terms( $args );
  echo '<nav id="menu">
  <label for="tm" id="toggle-menu">Navigation <span class="drop-icon">▾</span></label>
  <input type="checkbox" id="tm">
  <ul class="main-menu clearfix">';
  foreach ($product_cat as $parent_product_cat)
   {
  $child_args = array(
              'taxonomy' => 'product_cat',
              'hide_empty' => false,
              'parent'   => $parent_product_cat->term_id
          );
  $child_product_cats = get_terms( $child_args );
  if ($parent_product_cat->slug !== "%cf%87%cf%89%cf%81%ce%af%cf%82-%ce%ba%ce%b1%cf%84%ce%b7%ce%b3%ce%bf%cf%81%ce%af%ce%b1") {

    if (!empty($child_product_cats)) {
      echo '
      <li><a href="'.get_term_link($parent_product_cat->term_id).'">'.$parent_product_cat->name.'
          <span class="drop-icon">▾</span>
          <label title="Toggle Drop-down" class="drop-icon" for="'.$parent_product_cat->term_id.'">▾</label>
        </a>
        <input type="checkbox" id="'.$parent_product_cat->term_id.'">
        <ul class="sub-menu">';
      foreach ($child_product_cats as $child_product_cat)
      {
        echo '
        <li><a href="'.get_term_link($child_product_cat->term_id).'">'.$child_product_cat->name.'</a></li>
        ';
      }
      echo '</ul>
      </li>';
      } else {
        echo '<li><a href="'.get_term_link($parent_product_cat->term_id).'">'.$parent_product_cat->name.'</a></li>';
      }
  }
}
  echo '</ul>
</nav>';
}

add_action( 'storefront_header', 'ct_product_categories_as_menu', 50 );

/* Remove sidebar from ceratain pages */

function mm_remove_storefront_sidebar() {
	if (is_woocommerce() || is_page()) {
		remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
	}
}

add_action( 'get_header', 'mm_remove_storefront_sidebar');

/* display size chart after price */
function ct_show_sizechart() {
  global $product;
  echo '
  <!-- Trigger/Open The Modal -->
  <button id="myBtn">ΒΡΕΙΤΕ ΤΟ ΜΕΓΕΘΟΣ ΣΑΣ</button>

  <!-- The Modal -->
  <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
      <span class="close">&times;</span>
      <img src="'.site_url().'/wp-content/uploads/sizecharts/'.$product->get_sku().'.jpg" alt="">
    </div>

  </div>

  ';
}

add_action( 'woocommerce_single_product_summary', 'ct_show_sizechart', 12, 2 );

/**
	 * Display the custom credit
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_credit() {
		$links_output = '';

		if ( apply_filters( 'storefront_credit_link', false ) ) {
			$links_output .= '<a href="https://woocommerce.com" target="_blank" title="' . esc_attr__( 'WooCommerce - The Best eCommerce Platform for WordPress', 'storefront' ) . '" rel="author">' . esc_html__( 'Built with Storefront &amp; WooCommerce by M.M.', 'storefront' ) . '</a>.';
		}

		if ( apply_filters( 'storefront_privacy_policy_link', true ) && function_exists( 'the_privacy_policy_link' ) ) {
			$separator = '<span role="separator" aria-hidden="true"></span>';
			$links_output = get_the_privacy_policy_link( '', ( ! empty( $links_output ) ? $separator : '' ) ) . $links_output;
		}
		
		$links_output = apply_filters( 'storefront_credit_links_output', $links_output );
		?>
		<div class="site-info">
			<div class="site-info-credits">
				<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
			</div>
			<div class="site-info-payments">
				<ul class="site-info-payments-icons">
					<li class="payment-visa"><img src="<?php echo get_stylesheet_directory_uri() ?>/Icons/CardsIcons/Visa.jpg" alt="visa logo"></li>
					<li class="payment-mastercard"><img src="<?php echo get_stylesheet_directory_uri() ?>/Icons/CardsIcons/Mastercard.png" alt="mastercard logo"></li>
					<li class="payment-maestro"><img src="<?php echo get_stylesheet_directory_uri() ?>/Icons/CardsIcons/Maestro.png" alt="maestro logo"></li>
					<li class="payment-vbv"><a target="_blank" href="https://paycenter.piraeusbank.gr/redirection/Content/HTML/3DSecure_el.html"><img src="<?php echo get_stylesheet_directory_uri() ?>/Icons/VbV/vbv.jpg" alt="verified by visa logo"></a></li>
					<li class="payment-secure-code"><a target="_blank" href="https://paycenter.piraeusbank.gr/redirection/Content/HTML/3DSecure_el.html"><img src="<?php echo get_stylesheet_directory_uri() ?>/Icons/SecureCode/sc_68x37.gif" alt="secure code logo"></a></li>
				</ul>
			</div>
		</div><!-- .site-info -->
		<?php
	}